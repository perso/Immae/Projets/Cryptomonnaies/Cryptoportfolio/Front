package db

import "github.com/go-pg/pg"

type MarketConfig struct {
	Id         int64
	MarketName string `sql:",notnull"`
	UserId     int64  `sql:",notnull"`
	Config     map[string]string
}

func InsertMarketConfig(config *MarketConfig) error {
	return DB.Insert(config)
}

func GetUserMarketConfig(userId int64, market string) (*MarketConfig, error) {
	var config MarketConfig

	err := DB.Model(&config).Where("user_id = ?", userId).Where("market_name = ?", market).First()

	if err != nil && err != pg.ErrNoRows {
		return nil, err
	}

	if err == pg.ErrNoRows {
		return nil, nil
	} else {
		return &config, nil
	}
}

func SetUserMarketConfig(userId int64, market string, newConfig map[string]string) (*MarketConfig, error) {
	config := MarketConfig{
		UserId:     userId,
		MarketName: market,
		Config:     newConfig,
	}

	_, err := DB.Model(&config).
		OnConflict("(user_id, market_name) DO UPDATE").
		Set("config = ?", newConfig).
		Insert()

	return &config, err
}
