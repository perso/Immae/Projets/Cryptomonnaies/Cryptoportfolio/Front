package db

import (
	"strings"

	"github.com/go-pg/pg"
)

func PGCode(err error) string {
	if _, ok := err.(pg.Error); !ok {
		return ""
	}

	return err.(pg.Error).Field('C')
}

func IsDup(err error) bool {
	return PGCode(err) == "23505"
}

func IsSQLError(err error) bool {
	return strings.HasPrefix(err.Error(), "ERROR #")
}
