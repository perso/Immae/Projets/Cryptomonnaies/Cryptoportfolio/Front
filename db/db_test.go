package db

import "testing"

func TestInit(t *testing.T) {
	Init(DBConfig{"localhost:5432", "cryptoportfolio", "cryptoportfolio", "cryptoportfolio-dev"})
}

func TestUpdateUser(t *testing.T) {
	Init(DBConfig{"localhost:5432", "cryptoportfolio", "cryptoportfolio", "cryptoportfolio-dev"})
	t.Log(InsertUser(&User{Email: "j@test.com", PasswordHash: "yp"}))
	err := InsertUser(&User{Email: "t2@test.com", PasswordHash: "yp"})

	t.Log(err, IsDup(err))

	t.Log(GetUserByEmail("testyo"))
}

func TestMarketConfig(t *testing.T) {
	Init(DBConfig{"localhost:5432", "cryptoportfolio", "cryptoportfolio", "cryptoportfolio-dev"})

	config := MarketConfig{UserId: 1, MarketName: "poloniex"}
	config.Config = make(map[string]string)

	config.Config["secret"] = "key"

	t.Log(InsertMarketConfig(&config))
	t.Log(config)

	t.Log(GetUserMarketConfig(1, "poloniex"))

	config.Config["secret2"] = "key2"
	t.Log(SetUserMarketConfig(1, "poloniex", config.Config))
	t.Log(SetUserMarketConfig(1, "bifinance", config.Config))
}
