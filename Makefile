install:
	go get -u github.com/golang/dep/cmd/dep
	dep ensure
	go get github.com/aktau/github-release

clean:
	rm -rf vendor
