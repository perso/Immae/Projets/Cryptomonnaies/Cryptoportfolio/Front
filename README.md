# Cryptoportfolio app

Back and front end for cryptoportfolio project.

Server binaries and webapp files are shipped to Github. No need to install dev dependencies to deploy it. All you need is [ansible](http://docs.ansible.com/ansible/latest/intro_installation.html#installing-the-control-machine)

- [Deploy](#deploy) : deploy server binaries and webapp archive to a server with ansible
- [Server](#server) : server written in golang
- [Webapp](#webapp) : JS (React)
- [Release](#release) : release server binaries and webapp archive to github with ansible.

Dir tree
```
|- api/*.go   api endpoints
|- db/*.go    database models
|- cmd/
|   |--- app/
|         |-- Makefile
|         |-- conf.toml   server configuration file (dev)
|         |-- main.go     server entry point
|   |--- ansible/
|             |-- ansible.cfg
|             |-- hosts
|             |-- requirements.yml
|             |-- conf.toml.j2            server configuration file (prod)
|             |-- cryptoportfolio-app.j2  server systemd file
|             |-- vars.yml                playbook variables (vault)
|             |-- release.yml             release playbook
|             |-- deploy.yml              deploy playbook
|   |--- web/
|         |-- env/     app environments
|              |-- dev.env
|              |-- prod.env
|         |-- js/      js source code
|         |-- static/  index.html and style.css
```
## Server
#### Install
##### Golang installation (if needed)
1. Download right package at [https://golang.org/dl](https://golang.org/dl)
2. Install it
```
tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz

export PATH=$PATH:/usr/local/go/bin
```
#### Fetch dependencies
Simply run at the root of project :
```
  make install
```
#### Run locally
You need postgres running on `localhost:5432` and  the following setup:
```
CREATE USER cryptoportfolio;
ALTER USER cryptoportfolio WITH ENCRYPTED PASSWORD 'cryptoporfolio-dev';
ALTER ROLE cryptoportfolio WITH CREATEDB ;
CREATE DATABASE cryptoportfolio OWNER cryptoportfolio;
```
Then run the app in cmd/app:
```
  cd cmd/app
  make run
```
## Webapp
#### Install
You will need `npm`, `node` and `yarn`, then:
```
 cd cmd/web
 make install
```

#### Build local webapp
```
  make static ENV=dev
```
Files are now ready to be served by server.

## Ansible install requirements
```
cd cmd/ansible

ansible-galaxy install -r requirements.yml
```
## Releasing
To release, you need to have all dev environments setup for server and webapp. The token used to push to Github is in cmd/ansible/vars.yml. You need a password to decrypt this file. Ask me. You need to store this password in a file such as `~/.vault_cryptoportfolio_pass.txt`

You can then run the release.yml playbook, don't forget to provide a the release version :
```
  cd cmd/ansible

  ansible-playbook release.yml --extra-vars "version=X.X.X" --vault-password-file=~/.vault_cryptoportfolio_pass.txt
```
## Deploy
change `hosts` file :
```
[jloup-home]
jlj.am

[jloup-home:vars]
  ansible_port=21
  ansible_user=ansible-deploy # This user will be used to carry out operation on remote machine
                              # To make it work, this user should not be prompted for password when sudo
                              # else you need to pass --ask-sudo-pass when running playbook

  app_user=jloup    # user that will run the app service
  app_domain=jlj.am

  postgres_database=cryptoportfolio
  postgres_user=cryptoportfolio
  postgres_password=cryptoportfolio-dev

  linux_arch=386 # your machine arch. should be one if [386, amd64]
```

```
  cd cmd/ansible

  ansible-playbook deploy.yml --extra-vars "version=X.X.X" --vault-password-file=~/.vault_cryptoportfolio_pass.txt
```
