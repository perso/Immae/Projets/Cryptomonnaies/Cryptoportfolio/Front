'use strict';

var App     = require('./app.js');

var Api = {};

Api.API_HOST = process.env.API_HOST;
Api.API_PORT = process.env.API_PORT;

if (process.env.API_HTTPS === 'true') {
  Api.API_ROOT = 'https://';
} else {
  Api.API_ROOT = 'http://';
}

Api.API_ROOT += Api.API_HOST;
if (Api.API_PORT !== '80') {
  Api.API_ROOT += ':' + Api.API_PORT;
}

Api.API_ROOT += '/api';

var ApiEndpoints = {
  'SIGNUP': {
    'type': 'POST',
    'auth': false,
    'parameters': [
      {'name': 'email',    'mandatory': true, 'inquery': true},
      {'name': 'password', 'mandatory': true, 'inquery': true}
    ],
    'buildUrl': function(params) {
      return '/signup';
    }
  },
  'SIGNIN': {
    'type': 'POST',
    'auth': false,
    'parameters': [
      {'name': 'email',    'mandatory': true, 'inquery': true},
      {'name': 'password', 'mandatory': true, 'inquery': true}
    ],
    'buildUrl': function(params) {
      return '/signin';
    }
  },
  'MARKET': {
    'type': 'GET',
    'auth': true,
    'parameters': [
      {'name': 'name', 'mandatory': true, 'inquery': false},
    ],
    'buildUrl': function(params) {
      return '/market/' + params.name;
    }
  },
  'UPDATE_MARKET': {
    'type': 'POST',
    'auth': true,
    'parameters': [
      {'name': 'name',   'mandatory': true, 'inquery': false},
      {'name': 'key',    'mandatory': true, 'inquery': true},
      {'name': 'secret', 'mandatory': true, 'inquery': true},
    ],
    'buildUrl': function(params) {
      return '/market/' + params.name + '/update';
    }
  },
  'OTP_ENROLL': {
    'type': 'GET',
    'auth': true,
    'parameters': [],
    'buildUrl': function(params) {
      return '/otp/enroll';
    }
  },
  'OTP_VALIDATE': {
    'type': 'POST',
    'auth': true,
    'parameters': [
      {'name': 'pass', 'mandatory': true, 'inquery': true},
    ],
    'buildUrl': function(params) {
      return '/otp/validate';
    }
  },
};

Api.BuildRequest = function(endpointId, params) {
  var endpoint = ApiEndpoints[endpointId];
  var query    = {};
  var url      = '';
  var headers  = {};
  var jwt      = App.getUserToken();

  if (endpoint === undefined) {
    return {'err': 'cannot find endpoint ' + endpointId};
  }

  if (endpoint.auth === true && (jwt === undefined || jwt === null)) {
    return {'err': 'this endpoint needs auth'};
  }

  if (jwt !== undefined && jwt !== null) {
    headers.Authorization = 'Bearer ' + jwt;
  }

  for (var i = 0; i < endpoint.parameters.length; i++) {
    var parameter = endpoint.parameters[i];
    if (parameter.mandatory === true && params[parameter.name] === undefined) {
      return {'err': 'parameter \'' + parameter.name + '\' is mandatory'};
    }

    if (parameter.inquery === true) {
      query[parameter.name] = params[parameter.name];
    }
  }

  url = endpoint.buildUrl(params);

  return {'err': null, 'url': Api.API_ROOT + url, 'headers': headers, 'params': query, 'type': endpoint.type};
};

Api.Call = function(endpointId, params, fn) {
  var request = Api.BuildRequest(endpointId, params);

  if (request.err !== null) {
    console.error('Api BuildRequest error', request.err);
    fn({'err': request.err}, -1, {});
    return null;
  }

  return Api.DoRequest(request.type, request.url, request.params, request.headers, fn);
};

Api.DoRequest = function(type, url, params, headers, callback) {
  return $.ajax({
    data:     params,
    headers:  headers,
    timeout:  30000,
    dataType: 'json',
    error: function(xhr, status, err) {
      if (status === 'abort') {
        return;
      }

      var apiStatus   = null;
      var apiResponse = null;
      var apiCode     = null;
      if (xhr.responseJSON) {
        apiStatus   = xhr.responseJSON.status;
        apiResponse = xhr.responseJSON.response;
        apiCode     = xhr.responseJSON.code;
      }

      if (xhr.status === 401 || xhr.status === 403) {
        if (App.onUserNotAuthorized(xhr.status, apiCode) === false) {
          return;
        }
      }

      callback({'url': url, 'err': err, 'status': status, 'code': apiCode, 'xhr': xhr}, apiStatus, apiResponse);
    },
    success: function(data) {
      var err = null;
      if (data.status !== 'ok') {
        err = {'url': url, 'status': status};
      }
      callback(err, data.status, data.response);
    },
    type: type,
    url: url
  });
};

module.exports.Api = Api;

