'use strict';

var cookies = require('./cookies.js');
var page    = require('page');

var App = {};
var cookieExpire = 60 * 30;

App.errorCodeToMessage = function(code) {
  switch (code) {
    case 'invalid_email':
      return 'The email is not valid';
    case 'invalid_password':
      return 'The password is not valid';
    case 'email_exists':
      return 'This email is already registered';
    case 'invalid_credentials':
      return 'Invalid credentials';
    case 'invalid_otp':
      return 'Invalid code !';
    case 'user_not_confirmed':
      return 'Your account is being confirmed. Should be very soon !';
  }

  return code;
};

App.isUserSignedIn = function() {
  return cookies.hasItem('jwt');
};

App.getUserToken = function() {
  return cookies.getItem('jwt');
};

App.onUserSignIn = function(token) {
  if (!token || token === '') {
    page('/signin');
    return;
  }

  cookies.setItem('jwt', token, cookieExpire);
  page('/me');
};

App.onUserValidateOtp = function(token) {
  if (!token || token === '') {
    page('/signin');
    return;
  }

  cookies.setItem('jwt', token, cookieExpire);
  page('/me');
};

App.onUserSignUp = function(token) {
  if (!token || token === '') {
    page('/signin');
    return;
  }

  cookies.setItem('jwt', token, cookieExpire);
};

App.getUserJWT = function() {
  return cookies.getItem('jwt');
};

App.page = function(path, needsAuth, fn) {
  page(path, function(context) {
    if (needsAuth && !App.isUserSignedIn()) {
      page('/signin');
      return;
    }

    fn(context);
  });
};

App.go = function(path) {
  page(path);
};

App.start = function() {
  page();
};

App.onInternNavigation = function(href, e) {
  e.preventDefault();
  page(href);
};

App.onUserNotAuthorized = function(httpCode, apiCode) {
  switch (apiCode) {
    case 'not_authorized':
      cookies.removeItem('jwt');
      page('/signin');
      return false;
    case 'otp_not_setup':
      page('/otp/setup');
      return false;
    case 'need_otp_validation':
      page('/otp/validate');
      return false;
    default:
      return true;
  }
};

App.mount = function(app) {
  var root = React.createElement(
      'div',
      {className: 'container'},
      app
  );

  ReactDOM.unmountComponentAtNode(document.getElementById('app'));
  ReactDOM.render(root, document.getElementById('app'));
};

module.exports = App;
