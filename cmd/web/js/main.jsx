var SignupForm    = require('./signup.js').SignupForm;
var SigninForm    = require('./signin.js').SigninForm;
var OtpEnrollForm = require('./otp.js').OtpEnrollForm;
var PoloniexForm  = require('./poloniex.js').PoloniexForm;
var App           = require('./app.js');
var Api           = require('./api.js').Api;
var cookies       = require('./cookies.js');

var Logo = React.createClass({
  render: function() {
    return (<div id='logo'>
              <a href='/'>Cryptoportfolio</a>
            </div>);
  }
});

App.page('/signup', false, function(context) {
  if (App.isUserSignedIn()) {
    App.go('/me');
    return;
  }

  App.mount(
    <div>
      <Logo />
      <SignupForm onSuccess={App.onUserSignUp}/>
    </div>
  );
});

App.page('/signin', false, function(context) {
  if (App.isUserSignedIn()) {
    App.go('/me');
    return;
  }

  App.mount(
    <div>
      <Logo />
      <SigninForm onSuccess={App.onUserSignIn}/>
    </div>
  );
});

App.page('/signout', true, function(context) {
  cookies.removeItem('jwt');

  App.go('/');
});

App.page('/me', true, function(context) {
  Api.Call('MARKET', {'name': 'poloniex'}, function(err, status, data) {
    if (err) {
      console.error(err, data);
      return;
    }

    App.mount(
      <div>
        <Logo />
        <p>Poloniex</p>
        <PoloniexForm apiKey={data.key} apiSecret={data.secret}/>
      </div>
    );

  }.bind(this));
});

App.page('/otp/setup', true, function(context) {
  Api.Call('OTP_ENROLL', {}, function(err, status, data) {
    if (err) {
      console.error(err, data);
      return;
    }

    App.mount(
      <div>
        <Logo />
        <OtpEnrollForm onSuccess={App.onUserValidateOtp} img={'data:image/png;base64,' + data.base64img} secret={data.secret}/>
      </div>
    );

  }.bind(this));
});

App.page('/otp/validate', true, function(context) {
  App.mount(
    <div>
      <Logo />
      <OtpEnrollForm onSuccess={App.onUserValidateOtp} />
    </div>
  );
});

App.page('/', false, function(context) {
  App.go('/me');
});

$(document).ready(function() {
  App.start();
});
