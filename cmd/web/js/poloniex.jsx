var Api        = require('./api.js').Api;
var App        = require('./app.js');
var classNames = require('classnames');

module.exports.PoloniexForm = React.createClass({
  getInitialState: function() {
    return {'hideMsg': true, 'msg': '', 'msgOk': false, 'apiSecret': this.props.apiSecret, 'apiKey': this.props.apiKey};
  },
  handleSubmit: function(e) {
    Api.Call('UPDATE_MARKET', {'key': this.state.apiKey, 'secret': this.state.apiSecret, 'name': 'poloniex'}, function(err, status, data) {
      if (err) {
        console.error(err, data);
        this.displayMessage(App.errorCodeToMessage(err.code), false);
        return;
      }

      this.displayMessage('OK', true);

    }.bind(this));
    e.preventDefault();
  },
  handleApiKeyChange: function(event) {
    this.setState({'apiKey': event.target.value});
  },
  handleApiSecretChange: function(event) {
    this.setState({'apiSecret': event.target.value});
  },
  hideMessage: function() {
    this.setState({'hideMsg': true});
  },
  displayMessage: function(msg, ok) {
    this.setState({'msg': msg, 'msgOk': ok, 'hideMsg': false});
  },
  render: function() {
    var cName = classNames('form-message', {'hidden': this.state.hideMsg, 'message-ok': this.state.msgOk});
    return (
        <div className='row justify-content-center api-credentials-form'>
          <div className='col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-1 col-xs-10'>
            <form role='form' onSubmit={this.handleSubmit}>
              <input className='form-control' type='text' placeholder='apiKey' value={this.state.apiKey} onChange={this.handleApiKeyChange} />
              <input className='form-control' type='text' placeholder='apiSecret' value={this.state.apiSecret} onChange={this.handleApiSecretChange} />
              <input className='form-control submit' type='submit' value='Save' />
              <div className={cName} ref='message'>{this.state.msg}</div>
            </form>
          </div>
        </div>
       );
  }
});
