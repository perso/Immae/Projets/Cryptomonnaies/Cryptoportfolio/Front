---
- hosts: jloup-home

  tasks:
    - include_vars: vars.yml

    - name: install myservice systemd unit file
      template: src=cryptoportfolio-app.j2 dest=/etc/systemd/system/cryptoportfolio-app.service
      become: yes

    - name: stop cryptoportfolio-app
      systemd: state=stopped name=cryptoportfolio-app
      become: yes

    - name: Creates cryptoportfolio-app directory
      file: path=/var/cryptoportfolio-app state=directory owner={{ app_user }}
      become: yes

    - name: Set log file.
      file: path=/var/cryptoportfolio-app/app.log owner={{ app_user }} state=touch
      become: yes

    - name: Copy server app binary from github 'https://github.com/jloup/dist/releases/download/crypto-v{{ version }}/cryptoportfolio-linux-{{ linux_arch }}'.
      get_url:
        url: "https://github.com/jloup/dist/releases/download/crypto-v{{ version }}/cryptoportfolio-linux-{{ linux_arch }}"
        dest: /usr/bin/cryptoportfolio-app
        owner: "{{ app_user }}"
        mode: "u=rwx,g=r,o=r"
      become: yes

    - name: Copy server app configuration file. 
      template:
        src: conf.toml.j2
        dest: /var/cryptoportfolio-app/conf.toml
        owner: "{{ app_user }}"
      become: yes

    - name: Create webapp directory.
      file: path=/var/cryptoportfolio-app/static state=directory owner={{ app_user }}
      become: yes

    - name: Copy webapp files from github 'https://github.com/jloup/dist/releases/download/crypto-v{{ version }}/webapp.tar.gz'.
      unarchive:
        src: "https://github.com/jloup/dist/releases/download/crypto-v{{ version }}/webapp.tar.gz"
        dest: /var/cryptoportfolio-app/static
        remote_src: yes
        owner: "{{ app_user }}"
        mode: "u=rwx,g=r,o=r"
      become: yes

    - import_role:
        name: nginx
      become: yes
      vars:
        nginx_vhosts:
          - listen: "443 ssl"
            server_name: "{{ app_domain }}"
            filename: "{{ app_domain }}.443.conf"
            extra_parameters: |
              ssl_certificate /etc/letsencrypt/live/{{ app_domain }}/fullchain.pem;
              ssl_certificate_key /etc/letsencrypt/live/{{ app_domain }}/privkey.pem;
              location / {
                proxy_pass "http://127.0.0.1:8080";
              }

          - listen: "80"
            server_name: "{{ app_domain }}"
            filename: "{{ app_domain}}.80.conf"
            return: "301 https://{{ app_domain }}$request_uri"

    - import_role:
        name: certbot
      become: yes
      vars:
        certbot_admin_email: jeanloup.jamet@gmail.com
        certbot_create_if_missing: yes
        certbot_create_standalone_stop_services: []
        certbot_create_method: standalone
        certbot_certs:
          - domains:
            - "{{ app_domain }}"

    - name: Create postgres user.
      user: name=postgres

    - name: Add cryptoportfolio database.
      postgresql_db: name={{ postgres_database }}
      become: yes
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true

    - name: Add cryptoportfolio user.
      postgresql_user: user={{ postgres_user }} db={{ postgres_database }} password={{ postgres_password }}
      become: yes
      become_user: postgres
      vars:
        ansible_ssh_pipelining: true

    - file: path=/www/{{ app_user }} state=directory owner={{ app_user }}
      become: yes

    - name: start cryptoportfolio-app
      systemd: state=started name=cryptoportfolio-app daemon_reload=yes
      become: yes
