package api

import (
	"github.com/gin-gonic/gin"
	"github.com/jloup/utils"
)

var log = utils.StandardL().WithField("module", "api")

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		rawQuery := c.Request.URL.RawQuery

		c.Next()

		for _, err := range c.Errors {
			l := log.WithField("path", path)

			if rawQuery != "" {
				l = l.WithField("query", rawQuery)
			}

			l.Errorf("%s", err.Err)
		}
	}
}
