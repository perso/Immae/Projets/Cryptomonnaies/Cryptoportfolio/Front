package api

import (
	"fmt"

	"immae.eu/Immae/Projets/Cryptomonnaies/Cryptoportfolio/Front/db"
)

type MarketConfigQuery struct {
	In struct {
		User   db.User
		Market string
	}
}

func (q MarketConfigQuery) ValidateParams() *Error {
	if q.In.Market != "poloniex" {
		return &Error{BadRequest, "invalid market name", fmt.Errorf("'%v' is not a valid market name", q.In.Market)}
	}

	return nil
}

func (q MarketConfigQuery) Run() (interface{}, *Error) {
	config, err := db.GetUserMarketConfig(q.In.User.Id, q.In.Market)
	if err != nil {
		return nil, NewInternalError(err)
	}

	if config == nil {
		configMap := make(map[string]string)
		configMap["key"] = ""
		configMap["secret"] = ""

		config, err = db.SetUserMarketConfig(q.In.User.Id, q.In.Market, configMap)
		if err != nil {
			return nil, NewInternalError(err)
		}

	}

	return config.Config, nil
}

type UpdateMarketConfigQuery struct {
	In struct {
		User   db.User
		Market string
		Key    string
		Secret string
	}
}

func (q UpdateMarketConfigQuery) ValidateParams() *Error {
	if q.In.Market == "" {
		return &Error{BadRequest, "invalid market name", fmt.Errorf("'%v' is not a valid market name", q.In.Market)}
	}

	return nil
}

func (q UpdateMarketConfigQuery) Run() (interface{}, *Error) {
	configMap := make(map[string]string)
	if q.In.Key != "" {
		configMap["key"] = q.In.Key
	}
	if q.In.Secret != "" {
		configMap["secret"] = q.In.Secret
	}

	_, err := db.SetUserMarketConfig(q.In.User.Id, q.In.Market, configMap)
	if err != nil {
		return nil, NewInternalError(err)
	}

	return nil, nil
}
