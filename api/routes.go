package api

import (
	"encoding/base64"

	"github.com/gin-gonic/gin"
)

type Route struct {
	Method   string
	Handlers []gin.HandlerFunc
	Path     string
}

type Group struct {
	Root        string
	Middlewares []Middleware
	Routes      []Route
}

var Groups = []Group{
	{
		"",
		nil,
		[]Route{
			{"POST", []gin.HandlerFunc{Signup}, "/signup"},
			{"POST", []gin.HandlerFunc{Signin}, "/signin"},
		},
	},
	{
		"/otp",
		[]Middleware{JwtAuth, UserConfirmed},
		[]Route{
			{"GET", []gin.HandlerFunc{OtpEnrollment}, "/enroll"},
			{"POST", []gin.HandlerFunc{OtpValidate}, "/validate"},
		},
	},
	{
		"/market",
		[]Middleware{JwtAuth, UserConfirmed, OtpAuth},
		[]Route{
			{"GET", []gin.HandlerFunc{GetMarketConfig}, "/:name"},
			{"POST", []gin.HandlerFunc{UpdateMarketConfig}, "/:name/update"},
		},
	},
}

func Signup(c *gin.Context) {
	query := &SignupQuery{}

	query.In.Email = c.PostForm("email")
	query.In.Password = c.PostForm("password")

	RunQuery(query, c)
}

func Signin(c *gin.Context) {
	query := &SigninQuery{}

	query.In.Email = c.PostForm("email")
	query.In.Password = c.PostForm("password")

	RunQuery(query, c)
}

func OtpValidate(c *gin.Context) {
	query := &OtpValidateQuery{}

	query.In.Pass = c.PostForm("pass")
	query.In.User = GetUser(c)
	query.In.Claims = GetClaims(c)

	RunQuery(query, c)
}

func OtpEnrollment(c *gin.Context) {
	query := &OtpEnrollmentQuery{}

	query.In.User = GetUser(c)

	qrPng, secret, err := query.Run()
	if err != nil {
		WriteJsonResponse(ErrorResponse(err.Code, err.UserMessage), c)
		c.Error(err)
		return
	}

	if c.Query("format") == "png" {
		c.Header("X-OTP-Secret", secret)
		WriteBinary("image/png", qrPng.Bytes(), c)
	} else {
		response := struct {
			Base64img string `json:"base64img"`
			OtpSecret string `json:"secret"`
		}{
			base64.StdEncoding.EncodeToString(qrPng.Bytes()),
			secret,
		}

		WriteJsonResponse(SuccessResponse(response), c)
	}

}

func GetMarketConfig(c *gin.Context) {
	query := &MarketConfigQuery{}

	query.In.User = GetUser(c)
	query.In.Market = c.Param("name")

	RunQuery(query, c)
}

func UpdateMarketConfig(c *gin.Context) {
	query := &UpdateMarketConfigQuery{}

	query.In.User = GetUser(c)
	query.In.Market = c.Param("name")
	query.In.Key = c.PostForm("key")
	query.In.Secret = c.PostForm("secret")

	RunQuery(query, c)
}
