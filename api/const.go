package api

import "net/http"

//go:generate stringer -type=Status,ErrorCode -output const_string.go
type Status uint32
type ErrorCode uint32

const (
	OK Status = iota
	NOK

	BadRequest ErrorCode = iota + 1
	EmailExists
	InternalError
	InvalidCredentials
	InvalidEmail
	InvalidOtp
	InvalidPassword
	NeedOtpValidation
	NotAuthorized
	NotFound
	OtpAlreadySetup
	OtpNotSetup
	UserNotConfirmed
)

func StatusToHttpCode(status Status, code ErrorCode) int {
	if status == OK {
		return http.StatusOK
	}

	switch code {
	case BadRequest, InvalidPassword, InvalidEmail:
		return http.StatusBadRequest

	case InvalidCredentials, InvalidOtp:
		return http.StatusUnauthorized

	case UserNotConfirmed, NotAuthorized, OtpAlreadySetup, OtpNotSetup, NeedOtpValidation:
		return http.StatusForbidden

	case EmailExists:
		return http.StatusConflict

	case NotFound:
		return http.StatusNotFound
	}

	return http.StatusInternalServerError
}
